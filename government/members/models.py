from django.db import models
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _
from autoslug import AutoSlugField

from django.utils.text import slugify


class Person(models.Model):
    name = models.CharField(_("Name of Person"), blank=True, max_length=255)
    dob = models.DateField(_("Date of Birth"), blank=True, null=True)
    family_name = models.CharField(_("Surname"), blank=True, max_length=255)
    given_name = models.CharField(_("First Name"), blank=True, max_length=255)
    website = models.CharField(_("Website"), blank=True, max_length=255, null=True)
    twitter = models.CharField(_("Twitter"), blank=True, max_length=255, null=True)
    gender = models.CharField(max_length=10, default="")
    party = models.ForeignKey("Party", default=None, on_delete=models.CASCADE)
    slug = AutoSlugField(populate_from='name', unique="true")
    # faction = ManyToManyField("groups.Faction")

    def __str__(self):
        return self.name

    def __repr__(self):
        return self.name


class Member(models.Model):
    constituency = models.ForeignKey(
        "areas.Constituency", on_delete=models.CASCADE)
    cabinet = models.BooleanField(default=False)
    member_from = models.DateField(_("Date of first election"), default=None, null=True)
    person = models.ForeignKey(Person, on_delete=models.CASCADE, default=None)
    slug = AutoSlugField(populate_from="__str__", unique="true")
    
    def __str__(self):
        return self.person.name

    def party(self):
        return self.person.party.name

    def get_absolute_url(self):
        return reverse("members:detail", kwargs={"username": self.slug})


class Lord(models.Model):
    title = models.CharField(_("Name of Person"), blank=True, max_length=255)
    hereditary = models.BooleanField(default=False)
    person = models.ForeignKey(Person, on_delete=models.CASCADE, default=None)

    def get_absolute_url(self):
        return reverse("lords:detail", kwargs={"username": self.username})


class Vote(models.Model):
    date = models.DateField(_("Date of vote"))
    results = models.ManyToManyField(Member, related_name="vote")


class Election(Vote):
    participants = models.ManyToManyField(Member, related_name="election")


class House(models.Model):
    name = models.CharField(_("The name of the house"), max_length=255)


class Motion(Vote):
    house = models.ForeignKey(House, models.CASCADE)
    summary = models.TextField(_("A summary of the motion."))

    limit = models.Q(models.Q(app_label='members', model='Member') |
                     models.Q(app_label='members', model='Lord'))
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    sponsor = GenericForeignKey('content_type', 'object_id')


class Party(models.Model):
    name = models.CharField(max_length=30)

    def __str__(self):
        return self.name

    def __repr__(self):
        return self.name
