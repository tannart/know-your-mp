from django import forms

from .models import Member


class MemberChangeForm(forms.ModelForm):

    class Meta:
        model = Member
        fields = [
            'member_from']


class MemberCreationForm(forms.ModelForm):

    class Meta:
        model = Member
        fields = ['member_from']

    # def clean_username(self):
    #     name = self.cleaned_data["name"]

    #     try:
    #         Member.objects.get(name=name)
    #     except Member.DoesNotExist:
    #         return name

    #     raise ValidationError(self.error_messages["duplicate_name"])
