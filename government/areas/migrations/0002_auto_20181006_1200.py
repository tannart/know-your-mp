# Generated by Django 2.0.7 on 2018-10-06 12:00

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('areas', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='constituency',
            name='ended_date',
            field=models.DateField(blank=True, null=True, verbose_name='Date of removal'),
        ),
        migrations.AlterField(
            model_name='constituency',
            name='previous_constituency',
            field=models.CharField(max_length=255, null=True, verbose_name='Old Constituency name'),
        ),
    ]
