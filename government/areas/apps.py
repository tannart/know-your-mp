from django.apps import AppConfig


class AreasAppConfig(AppConfig):

    name = "government.areas"
    verbose_name = "Areas"

    def ready(self):
        try:
            import users.signals  # noqa F401
        except ImportError:
            pass
