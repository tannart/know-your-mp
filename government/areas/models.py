from autoslug import AutoSlugField

from django.db import models
from django.db.models import CharField, Model, DateField, ForeignKey, CASCADE
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _


class County(Model):
    name = CharField(_("Name of county"), blank=True, max_length=255)
    date_of_creation = DateField(_("Date of Birth"), blank=True)

    def get_absolute_url(self):
        return reverse("county:detail", kwargs={"name": self.username})


class Constituency(Model):
    name = CharField(_("Name of Constituency"), blank=True, max_length=255)
    constituency_id = models.IntegerField(_("Government assigned constituency id"))
    constituency_type = CharField(_("Type of constituency"), blank=True, max_length=255, null=True)
    ended_date = DateField(_("Date of removal"), blank=True, null=True)
    previous_constituency = CharField(_("Old Constituency name"), max_length=255, null=True)
    date_of_creation = DateField(_("Date of Creation"), blank=True, null=True)
    slug = AutoSlugField(populate_from='name', unique="true")

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("constituency:detail", kwargs={"name": self.username})


class City(Model):
    name = CharField(_("Name of county"), blank=True, max_length=255)
    date_of_creation = DateField(_("Date of Birth"), blank=True)

    def get_absolute_url(self):
        return reverse("city:detail", kwargs={"name": self.username})


class Country(Model):
    name = CharField(_("Name of county"), blank=True, max_length=255)
    capital = ForeignKey(City, on_delete=CASCADE)

    def get_absolute_url(self):
        return reverse("country:detail", kwargs={"name": self.username})
