from django.urls import path

from .views import (
    constituency_list_view,
    constituency_redirect_view,
    constituency_update_view,
    constituency_detail_view,
)

app_name = "constituency"

urlpatterns = [
    path("", view=constituency_list_view, name="list"),
    path("~redirect/", view=constituency_redirect_view, name="redirect"),
    path("~update/", view=constituency_update_view, name="update"),
    path("<slug:slug>/", view=constituency_detail_view, name="detail"),
]
