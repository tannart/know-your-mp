import requests
from time import sleep


DATA_PREFIX = "http://lda.data.parliament.uk"


def load_paginated_url(path, extract_fun):
    """Generic function for processing a paginated list of results from the government data site
    """
    current_page = requests.get(f"{DATA_PREFIX}{path}").json()['result']

    while 'next' in current_page:
        extract_fun(current_page)
        next_page = current_page['next']
        sleep(0.5)
        current_page = requests.get(next_page).json()['result']

    # Get the final page separately
    extract_fun(current_page)


def load_endpoint_url(path):
    return requests.get(f"{DATA_PREFIX}/{path}.json").json()["result"]["primaryTopic"]
