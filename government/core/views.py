import json
import pygeoip
from urllib.request import urlopen
from django.views.generic import TemplateView


class HomeView(TemplateView):
    template_name = "pages/home.html"

    def get(self, request, *args, **kwargs):
        user_ip = request.META["REMOTE_ADDR"]
        get_location_from_ip(user_ip)
        
        context = self.get_context_data(**kwargs)
        return self.render_to_response(context)

    def get_context_data(self, **kwargs):
        return {}


home = HomeView.as_view()


def get_location_from_ip(user_ip):
    GeoIPDatabase = "/app/government/GeoLiteCity.dat"
    ipData = pygeoip.GeoIP(GeoIPDatabase)
    record = ipData.record_by_name(user_ip)
    # print("The geolocation for IP Address %s is:" % user_ip)
    # print("Accurate Location: %s, %s, %s" % (record["city"], record["region_code"], record["country_name"]))
    # print("General Location: %s" % (record["metro_code"]))
