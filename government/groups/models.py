from django.db.models import CharField, Model, DateField
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _


class Party(Model):
    name = CharField(_("The name of the party"), max_length=255)
    created_at = DateField(_("Date of the parties inception"), blank=True)

    def get_absolute_url(self):
        return reverse("party:detail", kwargs={"name": self.name})


class Faction():
    name = CharField(
        _("The generally accepted name of a parliamentary faction"),
        blank=True,
        max_length=255)

    def get_absolute_url(self):
        return reverse("faction:detail", kwargs={"name": self.name})
